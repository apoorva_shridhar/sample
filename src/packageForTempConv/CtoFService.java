/**
 * 
 */
package packageForTempConv;

/**
 * @author apoorva
 *
 */
	
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.json.JSONException;
import org.json.JSONObject;

@Path("/ctofservice")
public class CtoFService {
	
	@GET
	//@Path("/sample")
	@Produces("application/json")
	
	public Response convertCtoF() throws JSONException {
		JSONObject jsonObject = new JSONObject();
		Double fahrenheit;
		Double celcius = 36.8;
		
		fahrenheit = ((celcius*9)/5)+32;
		
		jsonObject.put("F Value", fahrenheit);
		jsonObject.put("C Value", celcius);
		
		//String result = "@Produces(\"application/json\") Output: \n\nC to F converter output: \n\n" + jsonObject;
		return Response.status(200).entity(jsonObject.toString()).build();
	}
	
	@Path("{c}")
	@GET
	@Produces("application/json")
	
	public Response convertCtoFwithInput(@PathParam ("c") float c) throws JSONException {
		JSONObject jsonObject = new JSONObject();
		float fahrenheit;
		
		fahrenheit = ((c*9)/5)+32;
		
		jsonObject.put("F Value", fahrenheit);
		jsonObject.put("C Value", c);
		
		//String result = "@Produces(\"application/json\") Output: \n\nC to F converter output: \n\n" + jsonObject;
		return Response.status(200).entity(jsonObject.toString()).build();
	}
}